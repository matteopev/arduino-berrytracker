#include <Ethernet.h>
#include <EthernetUdp.h>
#include <SoftwareSerial.h>
#include <EEPROM.h>

//https://www.instructables.com/id/Arduino-IDE-16x-compiler-optimisations-faster-code/
//https://arduino.stackexchange.com/questions/14407/use-all-pins-as-digital-i-o



//#define AUTOCONF 1/
//#define LOGSETTINGS 1
//#define BRCDEBUG 1
//#define FEEDBACK_DEBUG 1
//#define NETDEBUG 1
//#define FEEDBACK_VERBOSE_DEBUG 1

//#define S2DEBUG 1

/*
  EEPROM 0-6  MAC Address
  7 DHCP enabled or not
  8-11  IP ADDRESS used if DHCP disabled
  50-53 SERVER IP address
  70-71 Server PORT
  15 - cycle
  16 - volume
  17 - LedBrigthness
  18-24 Station No
  100-136 UNIQUEIDENTIFIER
*/

//byte scanner_beep_vol_low[] = {0x07, 0xC6, 0x04, 0x08, 0x00, 0x8C, 0x02, 0xFE, 0x99  };
//byte scanner_beep_vol_med[] ={0x07, 0xC6, 0x04, 0x08, 0x00, 0x8C, 0x01, 0xFE, 0x9A  };
//byte scanner_no_interval[] = {0x07, 0xC6, 0x04, 0x08, 0x00, 0x89, 0x00, 0xFE, 0x9E };
//byte scanner_enable_beep_cmd[] = {0x07, 0xC6, 0x04, 0x08, 0x00, 0x38, 0x01, 0xFE, 0xEE };
//byte scanner_enable_handshake[] = {0x07, 0xC6, 0x04, 0x08, 0x00, 0x9F, 0x01, 0xFE, 0x87 };
//const byte scanner_request_revision[] = {0x04, 0xA3, 0x04, 0x00,  0xFF, 0x55 };

const byte version = 1;   /// version goes from 1.0 to 255.255
const byte version_sub = 9;

SoftwareSerial serialTTL_1 =  SoftwareSerial(9, 8);

//SoftwareSerial SerialDevice =  SoftwareSerial(A1, 5);


const byte scanner_disable_beep_cmd[] = {0x07, 0xC6, 0x04, 0x08, 0x00, 0x38, 0x00, 0xFE, 0xEF };
const byte scanner_beep_vol_high[] = {0x07, 0xC6, 0x04, 0x08, 0x00, 0x8C, 0x00, 0xFE, 0x9B  };
const byte scanner_always_on[] = {0x08, 0xC6, 0x04, 0x08, 0x00, 0xF2, 0x02, 0x01, 0xFE, 0x31   };
const byte scanner_always_power[] = {0x07, 0xC6, 0x04, 0x08, 0x00, 0x80, 0x00, 0xFE, 0xA7   };
const byte scanner_continuous_scanning[] = {0x07, 0xC6, 0x04, 0x08, 0x00, 0x8A, 0x04, 0xFE, 0x99   };
const byte scanner_500ms_interval[] = {0x07, 0xC6, 0x04, 0x08, 0x00, 0x89, 0x05, 0xFE, 0x99 };
const byte scanner_disable_handshake[] = {0x07, 0xC6, 0x04, 0x08, 0x00, 0x9F, 0x00, 0xFE, 0x88 };
const byte scanner_beep_1[] = {0x05, 0xE6, 0x04, 0x00, 0x00, 0xFF, 0x11 };
const byte scanner_beep_2[] = {0x05, 0xE6, 0x04, 0x00, 0x01, 0xFF, 0x10 };


byte LedBrigthness = 255; // brigthness ranges between 0=0V and 255=5V
byte CycleDelay = 5;   // 5*100 = 500ms
byte BuzzerVolume = 255; // volume ranges between 0=0V and 255=5V

char Station[] = "WHOAMI";  //default name

byte  feedback[5];          //it's our feedback array which we use for our feedbacks. All turned into byte!
byte LedState = LOW;        //used for controlling the feedbacks
unsigned long Led_STARTms = 0;  //used for feedbacks, to know when the feedback started


bool ScannerIsOnline = false;  //check if scanner is connected

/* BUFFER FOR SCANNER SERIAL PORT */
char inData[75];
char inChar = -1;
byte buff_offset = 0;

/* BUFFER FOR TCP IP*/
char TCPinData[120];
char TCPinChar = -1;
byte TCPbuff_offset = 0;


/* BUFFER FOR RFID SERIAL PORT */
char S2inData[75];
char S2inChar = -1;
byte S2buff_offset = 0;


byte mac[6] = { 0x90, 0xA2, 0xDA, 0x00, 0x00, 0x00 };
char Uniqueidentifier[37];
unsigned long tcp_net_check_last = 0;
unsigned long curr_ms = 0;

const byte Leds[3] = {3, 6, 7};//PIN3: RED, PIN6: GREEN; PIN 7: BLUE

byte server_ip[4] = { 0, 0, 0, 0 };
int server_port = 15004;

byte ip[4] = { 0, 0, 0, 0 };
bool dhcp = false;

#if defined(AUTOCONF)
EthernetUDP Udp;
#endif

EthernetClient client;


void setup() {

  //CONFIGURE PINOUT AND PERFORM BOOT SEQUENCE TO DETERMINE ALL LED AND BUZZER ARE FUNCTIONING
  pinMode(4, OUTPUT);
  pinMode(8, OUTPUT);

  for (byte x = 0; x < sizeof(Leds) ; x++) {
    pinMode(Leds[x], OUTPUT);
    analogWrite(Leds[x], LedBrigthness);
    delay(300);
    digitalWrite(Leds[x], LOW);
  }

  // pinMode(A0, OUTPUT);
  // analogWrite(A0, 1023);
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
  pinMode(A4, INPUT);
  pinMode(A5, INPUT);
  pinMode(5, INPUT);
  pinMode(12, INPUT);
  pinMode(13, INPUT);
  analogWrite(A0, 0);
  analogWrite(A1, 0);
  analogWrite(A2, 0);
  analogWrite(A3, 0);
  analogWrite(A4, 0);
  analogWrite(A5, 0);
  analogWrite(12, 0);
  analogWrite(13, 0);

  analogWrite(7, LedBrigthness);
  digitalWrite(9, LOW);
  pinMode(9, INPUT);
  Serial1.begin(115200);


  serialTTL_1.begin(9600);


  //  SerialDevice.begin(9600);
  GetDeviceSettings();
  ConfigureBarcodeScanner();
  byte ack[] = {0x05, 0xC7, 0x04, 0x00, 0x80, 0xFE, 0xB0};

  serialTTL_1.write(ack, sizeof(ack));   // CHECK IF SCANNER IS CONNECTED
  SetupNetwork();

#if defined(AUTOCONF)
  Udp.begin(15001);
#endif


}


void ConfigureBarcodeScanner() {

  serialTTL_1.write(scanner_beep_vol_high, sizeof(scanner_beep_vol_high));
  delay(200);
  serialTTL_1.write(scanner_continuous_scanning, sizeof(scanner_continuous_scanning));
  delay(200);
  serialTTL_1.write(scanner_always_power, sizeof(scanner_always_power));
  delay(200);
  serialTTL_1.write(scanner_always_on, sizeof(scanner_always_on));
  delay(200);
  serialTTL_1.write(scanner_disable_handshake, sizeof(scanner_disable_handshake));
  delay(200);
  serialTTL_1.write(scanner_500ms_interval, sizeof(scanner_500ms_interval));
  delay(200);
  serialTTL_1.write(scanner_disable_beep_cmd, sizeof(scanner_disable_beep_cmd));

}

void SetupNetwork() {
  if (EEPROM.read(0) == 0x90) {
    for (int i = 0; i < 6; i++) {
      mac[i] = EEPROM.read(i);
    }
  }
  else {


    int b = 0;
    EEPROM.write(0,  0x90);
    EEPROM.write(1, 0xA2);
    for (int c = 2; c < 6; c++) {
      b = random(0, 255);
      EEPROM.write(1 + c, b);
      mac[c] = b;
    }

    #if defined(NETDEBUG)
    Serial1.println("Temporary MAC Auto Generated (4 bytes)");
    #endif

    SaveMac();
  }

#if defined(NETDEBUG)
  Serial1.println("Initializing Ethernet...");
#endif

  if (dhcp) {

    Ethernet.begin(mac);
    #if defined(NETDEBUG)
      Serial1.print("IP = ");
      Serial1.println(Ethernet.localIP());
    #endif
  }
  else {

#if defined(NETDEBUG)
    Serial1.println("STATIC IP:" + String(ip[0]) + "." + String(ip[1]) + "." + String(ip[2]) + "." + String(ip[3]));
#endif
    Ethernet.begin(mac, ip);
  }
  while (Ethernet.hardwareStatus() == EthernetNoHardware) {
#if defined(NETDEBUG)
    Serial1.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
#endif
  }
}

void loop() {
  curr_ms =  millis();
  FeedManager();
  CheckNetwork();
  ReadBrc();
  // ReadSeralDevice();
  ReceiveTCP();
  delay(CycleDelay * 15);


}


/*
  void ReadSeralDevice() {
  if (SerialDevice.available()) {

    byte numBytesAvailable = SerialDevice.available();
    if (numBytesAvailable > 0) {
      int i;
      for (i = 0; i < numBytesAvailable; i++) {
        S2inChar = SerialDevice.read();


          Serial1.print("S2 Serial Receive:");
          Serial1.println(S2inData);


        S2inData[i + S2buff_offset] = S2inChar;
        if (S2inChar == 10) {
          S2inData[i + 1 + S2buff_offset] = '\0';
          S2buff_offset = 0;

          Serial1.print("Incoming BRC:");
          Serial1.println(S2inData);

          client.print("I2;" + String(Uniqueidentifier) + ";" + S2inData);
          break;
        }



      }
      if (inChar != 10 && inChar != -76 ) {

        Serial1.println(String(inData));

        buff_offset = buff_offset + i;

      }
    }
  }
  }

*/
void CheckNetwork() {
  if ((curr_ms - tcp_net_check_last) > 5000) {

    if (!ScannerIsOnline) {

#if defined(BRCDEBUG)
      Serial1.print("Scanner not connected!");
#endif
      analogWrite(3, 200);
      analogWrite(6, 90);

    }

#if defined(NETDEBUG)
    Serial1.print("*");
#endif
    client.println("*");
    tcp_net_check_last = curr_ms;
    if (Ethernet.linkStatus() == LinkOFF)  {

      analogWrite(7, LedBrigthness);
      client.stop();
      while (Ethernet.linkStatus() == LinkOFF) {
#if defined(NETDEBUG)
        Serial1.println("No network connection available.");
#endif
        delay(500);
      }
    }
    else if (!client.connected()) {

      analogWrite(7, LedBrigthness);
      if (EEPROM.read(1000) == 1) {


        if (client.connect(server_ip, server_port)) {
          client.println("INITINITIATE");
          client.println("REG;" + String(Uniqueidentifier) + ";" + String(Station) + "," + String(version) + "." + String(version_sub));


#if defined(NETDEBUG)
          Serial1.println("Connected Succesfully to Server");
#endif
        }
        else {
#if defined(NETDEBUG)
          Serial1.println("Link is up but cannot connect to server on provided IP,PORT:" + String(server_ip[0]) + "." + String(server_ip[1]) + "." + String(server_ip[2]) + "." + String(server_ip[3]) + "," + String(server_port) );
#endif
#if defined(AUTOCONF)
          ReceiveUDP();
#endif
        }
      }
      else {

        analogWrite(7, LedBrigthness);
#if defined(NETDEBUG)
        Serial1.println("Link is up, missing configuration on the Arduino. Waiting for broadcasting or Barcode Conf Scan");
#endif
#if defined(AUTOCONF)
        ReceiveUDP();
#endif
      }
    }

    else {



    }
  }
}

void GetDeviceSettings() {
  if (EEPROM.read(1000) == 1) {
#if defined(LOGSETTINGS)
    Serial1.println("EPROM Size: " + String(EEPROM.length()));
#endif
    EEPROM.get(100, Uniqueidentifier);

    for (int i = 8; i < 12; i++) {
      ip[i - 8] = EEPROM.read(i) ;
    }
    for (int i = 50; i < 54; i++) {
      server_ip[i - 50] = EEPROM.read(i) ;
    }
    dhcp = EEPROM.read(7);
    CycleDelay = EEPROM.read(15);
    BuzzerVolume = EEPROM.read(16);
    LedBrigthness = EEPROM.read(17);
    EEPROM.get(70, server_port);
    for (int i = 0; i < 7; i++) {
      Station[i] = EEPROM.read(i + 18) ;
    }
  }
#if defined(LOGSETTINGS)
  else {
    Serial1.println("Setting not found. Using default settings...");
  }
#endif

#if defined(LOGSETTINGS)
  Serial1.println("FIRMWARE VERSION: " + String(version) + "." + String(version_sub));
  Serial1.println("ID: " + String(Uniqueidentifier));
  Serial1.println("IP: " + String(ip[0]) + ":" + String(ip[1]) + ":" + String(ip[2]) + ":" + String(ip[3]));
  Serial1.println("SERVER IP: " + String(server_ip[0]) + ":" + String(server_ip[1]) + ":" + String(server_ip[2]) + ":" + String(server_ip[3]));
  Serial1.println("SERVER PORT: " + String(server_port));
  Serial1.println("DHCP: " + String(dhcp));
  Serial1.println("Station: " + String(Station));
  Serial1.println("CycleDelay (ms): " + String(CycleDelay * 15));
  Serial1.println("BuzzerVolume: " + String(BuzzerVolume));
  Serial1.println("LedBrigthness: " + String(LedBrigthness));
#endif
}

void ScannerSingleBeep() {
  serialTTL_1.write(scanner_beep_1, sizeof(scanner_beep_1));
}


void ScannerDoubleBeep() {
  serialTTL_1.write(scanner_beep_1, sizeof(scanner_beep_1));
  delay(50);
  serialTTL_1.write(scanner_beep_2, sizeof(scanner_beep_2));
}



void SettingsReceivedFeedback() {
  serialTTL_1.write(scanner_beep_1, sizeof(scanner_beep_1));
  delay(150);
  serialTTL_1.write(scanner_beep_2, sizeof(scanner_beep_2));
  delay(150);
  serialTTL_1.write(scanner_beep_2, sizeof(scanner_beep_2));
  delay(150);
  serialTTL_1.write(scanner_beep_2, sizeof(scanner_beep_2));
}

void ReadBrc() {
  if (serialTTL_1.available()) {
    //String data =  serialTTL_1.readStringUntil(10);
    byte numBytesAvailable = serialTTL_1.available();
    if (numBytesAvailable > 0) {
      int i;
      for (i = 0; i < numBytesAvailable; i++) {
        inChar = serialTTL_1.read();



        inData[i + buff_offset] = inChar;
        if (inChar == 10) {
          inData[i + 1 + buff_offset] = '\0';
          buff_offset = 0;
#if defined(BRCDEBUG)
          Serial1.print("Incoming BRC:");
          Serial1.println(inData);
#endif

          client.print("I1;" + String(Uniqueidentifier) + ";" + inData);
          if (inData[0] == '#')  {
#if defined(BRCDEBUG)
            Serial1.println("Settings Received!");
#endif
            ParseMessage(inData);
          }

          break;
        }

        else if (inChar == -76) {
          inData[i + 1 + buff_offset] = '\0';
          buff_offset = 0;
          ScannerIsOnline = true;
          digitalWrite(3, LOW);
          digitalWrite(6, LOW);
#if defined(BRCDEBUG)
          Serial1.println("BRC Scanner OK");

#endif

          memset(inData, 0, sizeof(inData));

          break;
        }

      }
      if (inChar != 10 && inChar != -76 ) {
#if defined(BRCDEBUG)
        // Serial1.println(String(inData));
#endif
        buff_offset = buff_offset + i;

      }
    }
  }
}

void ParseMessage(char input[]) {

  //  Serial1.println("Input buffer: " + String(input));
  bool feedback_received = 0;
  // Read each command pair
  char* command = strtok(input, ",");
  while (command != 0)
  {
    // Split the command in two values
    char* separator = strchr(command, ':');
    if (separator != 0)
    {
      *separator = 0;
      ++separator;


      if (command[0] == 'N') { //NEWID
#if defined(NETDEBUG)
        Serial1.println("New ID From Server: " + String(separator));
#endif

        for (int i = 0; i < 37; i++) {
          Uniqueidentifier[i] = separator[i];

        }

        EEPROM.put(100, Uniqueidentifier);

        client.stop();

      }
      else  if (command[0] == 'M') { //NEWMAC
        analogWrite(7, LedBrigthness);
        client.stop();
#if defined(NETDEBUG)
        Serial1.println("Mac from Server: " + String(separator));
#endif
        parseBytes(separator, ':', mac, 6, 16);
        SaveMac();

        SetupNetwork();
      }

      else if (command[0] == 'I') {
        feedback_received = 1;
        if (separator[0] == 'D') { // DHCP
          EEPROM.write(7 , 1 );
        }
        else {

          parseBytes(separator, '.', ip, 4, 10);



          for (int i = 8; i < 12; i++) {

            EEPROM.write(i, ip[i - 8] );
          }
          EEPROM.write(7 , 0 );
        }
      }
      else  if (command[0] == 'S') { //STATION NAME
        for (int i = 0; i < 7; i++) {
          EEPROM.write(i + 18, separator[i] );
        }
      }
      else   if (command[0] == 'X' )  { // RESET EEPROM
        SettingsReceivedFeedback();
        serialTTL_1.end();
        
        EEPROM_reset();
        client.stop();
       

        GetDeviceSettings();
         
        serialTTL_1.begin(9600);
        SetupNetwork();
      }


     else   if (command[0] == 'Y' )  { // RESET EEPROM
       // SettingsReceivedFeedback();
     //   serialTTL_1.end();
        
    //    EEPROM_reset();
        client.stop();
       
        GetDeviceSettings();
         
        
        SetupNetwork();
      }
      
      else  if (command[0] == 'B') {  // LED brigthness
        feedback_received = 1;
        EEPROM.write(17 ,  atoi(separator)  );
      }
      else  if (command[0] == 'T') {  // SERVER IP
        parseBytes(separator, '.', server_ip, 4, 10);
        for (int i = 50; i < 54; i++) {
          EEPROM.write(i, server_ip[i - 50] );
        }
        client.stop();
        feedback_received = 1;
      }
      else  if (command[0] == 'P') {   //SERVER TCP PORT
        feedback_received = 1;

        EEPROM.put(70, atoi(separator));
        client.stop();

      }
      else  if (command[0] == 'V') {  //VOLUME
        feedback_received = 1;
        EEPROM.write(16,  atoi(separator) );
      }
      else  if (command[0] == 'C') {   // CYCLE TIME
        EEPROM.write(15 , atoi(separator) );
        feedback_received = 1;
      }

#if defined(NETDEBUG)
      Serial1.println(String(command) + ":" + String(separator));
#endif
      // Do something with servoId and position
    }
    // Find the next command in input string
    command = strtok(0, ",");
  }
  if (feedback_received == 1) {
    ConfigureBarcodeScanner();

    SettingsReceivedFeedback();
    EEPROM.write(1000, 1);

    GetDeviceSettings();

  }



}

void parseBytes(const char* str, char sep, byte* bytes, int maxBytes, int base) {
  for (int i = 0; i < maxBytes; i++) {
    bytes[i] = strtoul(str, NULL, base);  // Convert byte
    str = strchr(str, sep);               // Find next separator
    if (str == NULL || *str == '\0') {
      break;                            // No more separators, exit
    }
    str++;                                // Point to next character after separator
  }
}

void ReceiveTCP() {

  int i = 0;
  while (client.available()) {
    TCPinChar = client.read();
    TCPinData[i + TCPbuff_offset] = TCPinChar;



    if (TCPinChar == 59) {
      TCPinData[i + 1 + TCPbuff_offset] = '\0';
      parseBytes(TCPinData, '.', feedback, 5, 10);
      i = -1;
      TCPbuff_offset = 0;
#if defined(FEEDBACK_DEBUG)
      Serial1.println("FEEDBACK: <" + String(TCPinData) + ">  F0:" + String(feedback[0]) + " F1:" + String(feedback[1]) + " F2:" + String(feedback[2]) + " F3:" + String(feedback[3]) + " F4:" + String(feedback[4]));
#endif
      memset(TCPinData, 0, sizeof(TCPinData));
      NewFeedReceived();
    }

    else if (TCPinChar == 10) {
      //   Serial1.println("BREAK");
      TCPinData[i + 1 + TCPbuff_offset] = '\0';
#if defined(FEEDBACK_DEBUG)
      Serial1.print("RESPONSE:");
      Serial1.println(String(TCPinData));
#endif
      ParseMessage(TCPinData);
      TCPbuff_offset = 0;
      break;
    }
    i++;
  }

  if ((TCPinChar != 10) & (i > 0)) {
#if defined(FEEDBACK_VERBOSE_DEBUG)
    Serial1.println("TCP Incoming PARTIAL " + String(TCPinData));
#endif
    TCPbuff_offset = i;
  }
}

void EEPROM_reset() {

#if defined(NETDEBUG)
  Serial1.println("Full Arduino Reset.");
#endif
  for (unsigned int i = 0 ; i < EEPROM.length() ; i++) {
    EEPROM.write(i, 0);
  }
}

void SaveMac() {
  for (int i = 0; i < 6; i++) {
    EEPROM.write(i, mac[i]);
  }
}


#if defined(AUTOCONF)
void ReceiveUDP() {
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    String broadcast_mx = Udp.readStringUntil(10);
    ParseMessage(broadcast_mx);
  }
}

#endif






void NewFeedReceived() {

  for (byte x = 0; x < sizeof(Leds) ; x++) {
    if (Leds[x] != feedback[0]) {

#if defined(FEEDBACK_VERBOSE_DEBUG)
      Serial1.println("Switching off PIN:" + String(x));
#endif
      digitalWrite(Leds[x], LOW);  // SWITCH OFF ALL THE OTHER FEEDBACKS
    }
    else {

#if defined(FEEDBACK_VERBOSE_DEBUG)
      Serial1.println("Turning on PIN:" + String(x));
#endif
      if (feedback[1] * 50 > 0) {
        curr_ms =  millis();
        Led_STARTms = curr_ms;
        analogWrite(Leds[x], LedBrigthness);
        LedState = HIGH;
      }
      else {
        digitalWrite(Leds[x], LOW);
        LedState = LOW;

      }
    }



  }

  if (feedback[4] == 2)  {
    ScannerDoubleBeep();
  }
  else if (feedback[4] == 1)  {
    ScannerSingleBeep();
  }
}


void FeedManager() {


  //  Serial1.println("CurrMS :"+String(curr_ms)+"   "+"Led_STARTms :"+String(Led_STARTms)+"   "+"feedback[1]*50 :"+String(feedback[1]*50)+"   "+"feedback[4] :"+String(feedback[4])+"   "+"(curr_ms >=  (Led_STARTms + feedback[1]*50) :"+String((curr_ms >=  (Led_STARTms + feedback[1]*50))+"   "+"(curr_ms <= (feedback[1]*50 + Led_STARTms)) :"+String((curr_ms <= (feedback[1]*50 + Led_STARTms))));
#if defined(FEEDBACK_VERBOSE_DEBUG)
  Serial1.println("CurrMS :" + String(curr_ms) + "   " + "Led_STARTms :" + String(Led_STARTms) + "   " + "feedback[1]*50 :" + String(feedback[1] * 50) + "   " + "feedback[3] :" + String(feedback[3]));
#endif

  // TURN ON THE LED AND DECREMENT THE REPETITION
  if ((curr_ms >=  Led_STARTms) & (curr_ms <= (feedback[1] * 50 + Led_STARTms)) & (feedback[3] > 0) & (LedState == LOW) ) {
    digitalWrite(feedback[0], HIGH);
    LedState = HIGH;

    Led_STARTms = curr_ms;
    feedback[3] = feedback[3] - 1;
  }

  else if ((curr_ms >=  (Led_STARTms + feedback[1] * 50)) & (LedState == HIGH))  {
    digitalWrite(feedback[0], LOW);
    LedState = LOW;
    if (feedback[3] > 0 ) {
      Led_STARTms = curr_ms + feedback[2] * 50;
    }

  }

}
