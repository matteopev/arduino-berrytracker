GO

/****** Object:  Table [dbo].[Device]    Script Date: 19/02/2019 10:21:05 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Device](
	[DeviceKey] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[Station] [varchar](100) NULL,
	[Command] [varchar](50) NULL,
	[Param] [varchar](50) NULL,
	[MacAddress] [varchar](17) NULL,
	[IpAddress] [varchar](15) NULL,
	[DHCP] [bit] NULL,
	[LastSeen] [datetime] NULL,
	[LedBrightness] [int] NULL,
	[BuzzerVolume] [int] NULL,
	[Response] [varchar](100) NULL,
	[Feedback] [varchar](16) NULL,
	[BootTime] [datetime] NULL,
	[NetCheckInterval] [int] NULL,
	[AckInterval] [int] NULL,
	[LoopInterval] [int] NULL,
	[FirmwareVersion] [varchar](10) NULL,
	[LastModified] [datetime] NULL,
	[EmployeeKey] [uniqueidentifier] NULL,
	[LastLogon] [datetime] NULL,
 CONSTRAINT [PK_Device] PRIMARY KEY CLUSTERED 
(
	[DeviceKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Device] ADD  CONSTRAINT [DF_Devices_DeviceKey]  DEFAULT (newid()) FOR [DeviceKey]
GO

ALTER TABLE [dbo].[Device] ADD  CONSTRAINT [DF_Devices_DHCP]  DEFAULT ((1)) FOR [DHCP]
GO

ALTER TABLE [dbo].[Device] ADD  CONSTRAINT [DF_Devices_NetCheckInterval]  DEFAULT ((4000)) FOR [NetCheckInterval]
GO

ALTER TABLE [dbo].[Device] ADD  CONSTRAINT [DF_Devices_AckInterval]  DEFAULT ((4000)) FOR [AckInterval]
GO

ALTER TABLE [dbo].[Device] ADD  CONSTRAINT [DF_Devices_LoopInterval]  DEFAULT ((0)) FOR [LoopInterval]
GO

ALTER TABLE [dbo].[Device] ADD  CONSTRAINT [DF_Devices_LastModified]  DEFAULT (getdate()) FOR [LastModified]
GO


