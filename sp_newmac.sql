/****** Object:  StoredProcedure [dbo].[sp_Device_NewMAC]    Script Date: 5/03/2019 10:11:19 PM ******/
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO



CREATE PROCEDURE [dbo].[sp_Device_NewMAC] 
(

	@Prefix varchar(5),
	@Mac varchar(100) out
	
)
AS
begin

set @Mac = ''
		
while ((select count(*) from Device where MacAddress = @Mac)<>0 or (Len(@Mac)=0)) begin
	


			  select
					@Mac =
       
					@Prefix+':' + 
					substring(x,(abs(checksum(newid()))%15)+1,1)+
					substring(x,(abs(checksum(newid()))%15)+1,1)+
			':' + 
					/* and so on for as many characters as needed */
					substring(x,(abs(checksum(newid()))%15)+1,1)+
					substring(x,(abs(checksum(newid()))%15)+1,1)+
			':' + 
					substring(x,(abs(checksum(newid()))%15)+1,1)+
					substring(x,(abs(checksum(newid()))%15)+1,1)+
			':' + 
					substring(x,(abs(checksum(newid()))%15)+1,1)+
					substring(x,(abs(checksum(newid()))%15)+1,1)+
			':' + 
					substring(x,(abs(checksum(newid()))%15)+1,1)+
					substring(x,(abs(checksum(newid()))%15)+1,1)

			from
					  (select x='0123456789ABCDE') a
			cross apply (select top 1 row_number() over(order by id) As RW from syscolumns) b

end


end


GO


