GO
/****** Object:  StoredProcedure [dbo].[sp_BerryTracking]    Script Date: 19/02/2019 9:43:52 AM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

ALTER PROCEDURE [dbo].[sp_BerryTracking] 
(
	@DeviceKey uniqueidentifier,
	@MacAddress varchar(17),
	@IpAddress varchar(15), 
	@Command varchar(50),
	@Param varchar(50),
	@Response varchar(100) out,
	@Feedback varchar(16) out
)
AS
begin

declare @NewMac varchar(17)=''

declare @Station varchar(6)=''
declare @EmployeeKey uniqueidentifier
set @Response = 'R:NO RESPONSE SET STILL'

/* 

COMMAND SHORTCUT
EVERYTHING WORKS IN THE FOLLOWING WAY:   KEY:VAL,KEY:VAL

MEANS THAT WITH ONE RESPONSE WE CAN SEND MULTIPLE COMMANDS TO THE DEVICE

COMMANDS ARE THE SAME TROUGHT BARCODING AND TCP-IP, MEANS WE CAN CHANGE SETTING IN THE DATABASE DIRECTLY AND NEXT TIME CLIENT LOGON WILL TAKE THOSE NEW SETTINGS

R --> RESPONSE MESSAGE FROM SERVER, SINCE NO DISPLAY, USED FOR DEBUGGIN ONLY
N --> SAVE NEW UUID FROM SERVER
M --> SAVE NEW MAC FROM SERVER
I --> CLIENT IP SETTINGS  (D=DHCP)
S --> STATION NAME. 6 CHAR
X--> RESET ENTIRE EEPROM
B --> LED BRIGTHNESS FROM 0=0V TO 255=5V
T --> SERVER IP 
P --> SERVER PORT
V --> BUZZER VOLUME FROM 0=0V TO 255=5V
C --> CYCLE DELAY (FROM 0 TO 255)*25ms.  1=25ms, 2=50ms and so on.


FEEDBACK MESSAGES IS IN THE FOLLOWING FORM. MEANS YOU CAN DECIDE WITH PIN TO TURN ON FROM THE STORED PROCEDURE! THE DEVICE IS IGNORANT, DOESN'T HAVE THE CONCEPT OF WHICH PIN IS WHAT. IT DOES WHAT YOU TELL TO DO. 
BUZZER IS AT THE MOMENT ONE EXCEPTION, SINCE WE ARE USING THE SCANNER BUZZER AND THERE IS ONLY 1 COMMAND WE CAN SEND. WE SHOULD HAVE OUR OWN SIREN IN THE FUTURE., WITH DIFFERENT TONE, AND WE CAN ALSO REGULATE THE VOLUME.

PIN,REPEAT,INTERVAL,DELAY
FOLLOWING CURRENT PINOUT LED: R=3,G=6,B=7, 0=None
DURATION: xxx  (x 50ms)
INTERVAL: xxx (x 50ms)
REPETITION: XXX
BUZZER: 1,0  At the moment we don't have a buzzer. We can emit a sound from the scanner. We cannot specify duration or volume. 1=Emit a sound.

EXAMPLE: "78.500.200.577.1"

*/
BEGIN TRY 
insert into Adv_Log values(GETDATE(), 'Berry Tracking' , '@Command', convert(varchar(100), @Command), 1) 
insert into Adv_Log values(GETDATE(), 'Berry Tracking' , '@Param', convert(varchar(100), @Param), 1) 


--record the last action performed by the device. If idle for long time and packer is logged in, we can automatically logoff packer
update Device set LastSeen = GETDATE(), Command = @Command, Param = @Param where DeviceKey = @DeviceKey

  if(@Command = "I1") begin    -- INPUT 1 IS THE BARCODE SERIAL
	
	-- DO YOUR THING USING @Param. You can Log in a packer for example based on barcode prefix. ETC.  Then, set your feedback.
		set @Feedback =  "6.8.8.10.2"; 
		
		
		if (SUBSTRING(@Param, 1,2) = "$P") begin
			
			-- LOG THE PACKER  IN, RECORD THE LAST LOGIN TIME. LOGOFF PACKER FROM OTHER STANDS.
			set @EmployeeKey =(SELECT EmployeeKey from Employee where Code = SUBSTRING(@Param, 3,LEN(@Param)))
			update device set LastLogon = GETDATE(), EmployeeKey = @EmployeeKey where DeviceKey = @DeviceKey

			update device set EmployeeKey = null where EmployeeKey =@EmployeeKey and DeviceKey <> @DeviceKey
			
			set @Feedback =  "6.10.0.0.2"; 
			set @Response = "R:PACKER "+SUBSTRING(@Param, 3,LEN(@Param))+" LOGGED IN." 
		
		end 
  end
  
  else if(@Command = "I2") begin    -- INPUT 2 IS THE DATA FROM THE RFID
	
	set @Response = "R:BLOCK NOT IMPLEMENTED" 
	set @Feedback =  "3.20.0.0.1";

  
  end
  
  else if(@Command = "I3") begin    -- INPUT 3 IS THE DATA FROM SOMETHING ELSE -- NEED FEASIBILITY STUDY.
	set @Response = "R:BLOCK NOT IMPLEMENTED" 
	set @Feedback =  "3.20.0.0.1";  
  end
  else if(@Command = "REG") begin
	
	if not exists (select * from Device where DeviceKey = @DeviceKey) BEGIN

	
		insert into Device values (@DeviceKey, "WHOAMI", @Command, @Param, @MacAddress, @IpAddress, 1, GETDATE(), 255, 255, @Response, @Feedback, GETDATE(), 5000, 5000, 0, 1, GETDATE(), null, null)
		--set @Feedback =  "00000.00.0000.0000-02000.00.0000.00000-1";  -- turn on green for 2 seconds
		set @Response = "N:"+CONVERT(VARCHAR(36), @DeviceKey)+",R:Registered." 
		set @Feedback =  "6.20.0.0.1";

	end


	else if exists (select * from Device where MacAddress = @MacAddress and DeviceKey <> @DeviceKey) begin
		
		EXEC [dbo].[sp_Device_NewMAC]  '90:A2' ,@NewMac OUTPUT
			 
		set @Feedback =  "7.255.0.0.1";
		set @Response = "M:"+@NewMac+",R:MAC Address already in use." 

	end
	else if exists (select * from Device where DeviceKey = @DeviceKey) begin
	
		if exists (select * from Device where MacAddress <> @MacAddress and DeviceKey = @DeviceKey) begin
			-- UPDATE MAC ADDRESS
			update Device set MacAddress = @MacAddress where DeviceKey = @DeviceKey
			set @Response = "R:Device has a new MAC" 
			set @Feedback =  "6.20.0.0.1";

		end
		else begin 
			set @Response = "R:Device already registered. Success."
			set @Feedback =  "6.20.0.0.1"; 
		end
	end



	set @Station = SUBSTRING(@Param, 0, CHARINDEX(',', @Param) ) 

	UPDATE DEVICE  set Station = 'WHOAMI' where Station = @Station   --SET THE OTHER DEVICES AS UNKNOWN STATION
	
	update Device set BootTime = GETDATE(), FirmwareVersion = SUBSTRING(@Param, CHARINDEX(',', @Param) + 1, LEN(@Param)),  Station= @Station where DeviceKey = @DeviceKey
	
	
  end
  else begin
	set @Response = "R:Unknown Command." 
	set @Feedback =  "6.20.0.0.1";
  end 
 
 
 update Device set Feedback =@Feedback, Response = @Response where DeviceKey = @DeviceKey
 insert into Adv_Log values(GETDATE(), 'Berry Tracking' , '@Response', convert(varchar(100), @Response), 1) 


 END TRY  
BEGIN CATCH  
 insert into Adv_Log values(GETDATE(), 'Berry Tracking' , '@ERROR_MESSAGE()',convert(varchar(100), ERROR_LINE())+":"+ convert(varchar(100), ERROR_MESSAGE()), 1) 
END CATCH;   
  

end


